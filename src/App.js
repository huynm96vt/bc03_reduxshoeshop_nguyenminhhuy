import logo from "./logo.svg";
import "./App.css";
import "antd/dist/antd.css";
import ShoesShopRedux from "./BTShoeShopRedux/ShoesShopRedux";

function App() {
  return (
    <div className="App">
      <ShoesShopRedux />
    </div>
  );
}

export default App;
