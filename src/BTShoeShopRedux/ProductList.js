import React, { Component } from "react";
import { connect } from "react-redux";
import ProductItem from "./ProductItem";

class ProductList extends Component {
  render() {
    return (
      <div className="container py-5">
        <div className="row">
          {this.props.productList.map((item, index) => {
            return <ProductItem data={item} key={index} />;
          })}
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    productList: state.shoeShopReducer.productList,
  };
};

export default connect(mapStateToProps)(ProductList);
