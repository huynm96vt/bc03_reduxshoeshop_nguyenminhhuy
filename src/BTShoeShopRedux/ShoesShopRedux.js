import React, { Component } from "react";
import { connect } from "react-redux";
import Cart from "./Cart";
import { dataShoeShop } from "./dataShoeShop";
import ProductList from "./ProductList";

class ShoesShopRedux extends Component {
  render() {
    return (
      <div>
        <ProductList />

        <h4>Số lượng sản phẩm trong giỏ hàng: {this.props.gioHang.length}</h4>
        {this.props.gioHang.length > 0 && <Cart />}
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    gioHang: state.shoeShopReducer.gioHang,
  };
};

export default connect(mapStateToProps)(ShoesShopRedux);
