import React, { Component } from "react";
import { connect } from "react-redux";
import ModalConfirm from "./ModalConfirm";
import { TANG_GIAM_SO_LUONG, XOA_SAN_PHAM } from "./redux/constants/constants";

class Cart extends Component {
  render() {
    return (
      <div className="container">
        <ModalConfirm
          isOpenModal={this.props.isOpenModal}
          handleOpenModal={this.props.handleOpenModal}
          handleCloseModal={this.props.handleCloseModal}
        />
        <table className="table">
          <thead>
            <td>ID</td>
            <td>Tên sản phẩm</td>
            <td>Giá sản phẩm</td>
            <td>Số lượng</td>
            <td>Thao tác</td>
          </thead>
          <tbody>
            {this.props.gioHang.map((item) => {
              return (
                <tr>
                  <td>{item.id}</td>
                  <td>{item.name}</td>
                  <td>{item.price}</td>
                  <td>
                    <button
                      onClick={() => {
                        this.props.handleTangGiamSoLuong(item.id, 1);
                      }}
                      className="btn btn-success"
                    >
                      Tăng
                    </button>
                    <span className="mx-2">{item?.soLuong}</span>
                    <button
                      className="btn btn-secondary"
                      onClick={() => {
                        this.props.handleTangGiamSoLuong(item.id, -1);
                      }}
                    >
                      Giảm
                    </button>
                  </td>

                  <td>
                    <button
                      onClick={() => {
                        this.props.handleXoaSanPham(item.id);
                      }}
                      className="btn btn-danger"
                    >
                      Xoá
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    gioHang: state.shoeShopReducer.gioHang,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleTangGiamSoLuong: (id, giaTri) => {
      dispatch({
        type: TANG_GIAM_SO_LUONG,
        payload: { id, giaTri },
      });
    },
    handleXoaSanPham: (id) => {
      dispatch({
        type: XOA_SAN_PHAM,
        payload: id,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
