import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_TO_CART } from "./redux/constants/constants";

class ProductItem extends Component {
  render() {
    let { name, price, image } = this.props.data;
    return (
      <div className="card col-3">
        <img src={image} className="card-img-top" alt="..." />
        <div className="card-body">
          <h5 className="card-title">{name}</h5>
          <p className="card-text">{price}</p>
          <a
            className="btn btn-warning"
            onClick={() => {
              this.props.handleThemSanPham(this.props.data);
            }}
          >
            Add to cart
          </a>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleThemSanPham: (sanPham) => {
      dispatch({
        type: ADD_TO_CART,
        payload: sanPham,
      });
    },
  };
};

export default connect(null, mapDispatchToProps)(ProductItem);
