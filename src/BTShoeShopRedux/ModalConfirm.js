import React, { Component } from "react";
import { Button, Modal } from "antd";
import { connect } from "react-redux";
import { CLOSE_MODAL, OPEN_MODAL } from "./redux/constants/constants";

class ModalConfirm extends Component {
  render() {
    return (
      <Modal
        title="Bạn muốn xoá sản phẩm này ?"
        visible={this.props.isOpenModal}
        onOk={this.props.handleOpenModal}
        onCancel={this.props.handleCloseModal}
      ></Modal>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    isOpenModal: state.shoeShopReducer.isOpenModal,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleOpenModal: () => {
      dispatch({
        type: OPEN_MODAL,
      });
    },
    handleCloseModal: () => {
      dispatch({
        type: CLOSE_MODAL,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ModalConfirm);
